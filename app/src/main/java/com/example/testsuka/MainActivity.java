package com.example.testsuka;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private BluetoothAdapter btAdapter;
    Intent btEnableIntent;
    int requestCodeForEnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        btEnableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        requestCodeForEnable = 1;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == requestCodeForEnable) {
            if (requestCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), "Blutooth is enable", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Blutooth Enabling Cancelled", Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {
        if (btAdapter == null) {
            Toast.makeText(getApplicationContext(), "Blutooth not support", Toast.LENGTH_LONG).show();
        } else {
            if (!btAdapter.isEnabled()) {
                startActivityForResult(btEnableIntent, requestCodeForEnable);
            }
            Intent i = new Intent(MainActivity.this, change_bt.class);
            startActivity(i);
            finish();
        }
    }
}